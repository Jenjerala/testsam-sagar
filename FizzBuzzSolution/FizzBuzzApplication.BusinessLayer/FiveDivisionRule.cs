﻿// ---------------------------------------------------------------------------
// <copyright file="FiveDivisionRule.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//         Division by five functionalities.
// </summary>
// ---------------------------------------------------------------------------
namespace FizzBuzzApplication.BusinessLayer
{
    using System;
    using System.Globalization;

    /// <summary>
    /// class to verify five division numbers.
    /// </summary>
    public class FiveDivisionRule : IDivisionRule
    {
        /// <summary>
        /// Check the number is divisible by five.
        /// </summary>
        /// <param name="number">user input</param>
        /// <param name="day">Day Of Week</param>
        /// <returns>string result</returns>
        public string DivisionProcess(int number, DayOfWeek day)
        {
            if (day != DayOfWeek.Wednesday)
            {
                return number % 5 == 0 ? FizzBuzzResources.Buzz : number.ToString(CultureInfo.CurrentCulture);
            }
            else
            {
                return number % 5 == 0 ? FizzBuzzResources.Wuzz : number.ToString(CultureInfo.CurrentCulture);
            }
        }
    }
}