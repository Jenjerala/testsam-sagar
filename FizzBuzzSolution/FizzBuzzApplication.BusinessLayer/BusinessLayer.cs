﻿// ---------------------------------------------------------------------------
// <copyright file="BusinessLayer.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
////<summary>
//  BusinessLayer for FizzBuzz process.
// </summary>
// ---------------------------------------------------------------------------

namespace FizzBuzzApplication.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// class  used for generation for <c>fizzbuzz</c> sequence
    /// </summary>
    public class BusinessLayer : IBusinessLayer
    {
        /// <summary>
        /// Local variable declaration List DivisionRule.
        /// </summary>
        private readonly List<IDivisionRule> listDivisionsRules;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessLayer" /> class.
        /// </summary>
        /// <param name="listDivisibleRules">list of divisions</param>
        public BusinessLayer(List<IDivisionRule> listDivisibleRules)
        {
            this.listDivisionsRules = listDivisibleRules;
            this.CurrentDay = DateTime.Now.DayOfWeek;
        }

        /// <summary>
        /// Gets or sets Property to set Current Day of the week.
        /// </summary>
        public DayOfWeek CurrentDay
        {
            get;
            set;
        }

        /// <summary>
        /// generates the <c>fizzbuzz</c> sequence
        /// </summary>
        /// <param name="inputNumber">user input</param>
        /// <returns>list of strings : <c>fizzbuz</c>  sequence</returns>
        public List<string> GenerateFizzBuzzSequence(int inputNumber)
        {
            var fizzbuzzCollection = new List<string>();
            for (int counter = 1; counter <= inputNumber; counter++)
            {
                string divisibleOutput = null;
                foreach (var divisibleRule in this.listDivisionsRules)
                {
                    divisibleOutput = divisibleRule.DivisionProcess(counter, this.CurrentDay);
                    if (divisibleOutput != counter.ToString(CultureInfo.CurrentCulture))
                    {
                        break;
                    }
                }

                fizzbuzzCollection.Add(divisibleOutput);
            }

            return fizzbuzzCollection;
        }
    }
}
